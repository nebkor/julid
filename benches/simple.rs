use divan::black_box;
use julid::Julid;

fn main() {
    divan::main();
}

#[divan::bench]
fn jbench() {
    for _ in 0..1_000_000 {
        let x = black_box(Julid::new());
        if x < 1u128.into() {
            println!("that's weird");
        }
    }
}
