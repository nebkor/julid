use clap::Parser;
use julid::Julid;

#[derive(Debug, Parser)]
#[command(
    author,
    version = "1.618033988",
    about = "Generate, print, and parse Julids"
)]
struct Cli {
    #[clap(
        help = "Print the components of the given Julid",
        short = 'd',
        long = "decode"
    )]
    pub input: Option<String>,
    #[clap(help = "Number of Julids to generate", default_value_t = 1)]
    pub num: usize,
    #[clap(
        help = "The answer to the meaning of Julid",
        default_value_t = false,
        short,
        long
    )]
    pub answer: bool,
}

fn main() {
    let cli = Cli::parse();
    if let Some(ts) = cli.input {
        if let Ok(ts) = Julid::from_str(&ts) {
            println!("Created at:\t\t{}", ts.created_at());
            println!("Monotonic counter:\t{}", ts.counter());
            println!("Random:\t\t\t{}", ts.random());
        } else {
            eprintln!("Could not parse input '{}' as a Julid", ts);
            std::process::exit(1);
        }
    } else {
        // Just print some Julids
        let num = cli.num;
        for _ in 0..num {
            let j = if cli.answer {
                42u128.into()
            } else {
                Julid::new()
            };
            println!("{j}");
        }
    }
}
